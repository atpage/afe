#!/usr/bin/env python3

import unittest

import datetime as dt

import pandas as pd
import numpy as np

from afe import AFE

class TestClass(unittest.TestCase):

    def setUp(self):
        sr = 10
        samples = sr*60*5
        xyz = np.random.randn(samples, 3)
        self.df = pd.DataFrame(
            data    = xyz,
            columns = ['x','y','z'],
            index   = np.arange(0,samples/sr,1/sr)
        )

    # TODO: test(s) with "real" data (self.df)

    def test_create_AFE(self):
        # Timestamp index:
        idx = pd.DatetimeIndex(data=[dt.datetime.now(),dt.datetime.now()+dt.timedelta(seconds=0.05)])
        df = pd.DataFrame(columns=['x','y','z','rx','ry','rz'], index=idx)
        afe = AFE(df)

        # Float index:
        idx = pd.Float64Index([0.0, 0.05])
        df = pd.DataFrame(columns=['x','y','z','rx','ry','rz'], index=idx)
        afe = AFE(df)

        # No (explicit) index:
        with self.assertRaises(TypeError):
            df = pd.DataFrame(columns=['x','y','z','rx','ry','rz'])
            afe = AFE(df)

        # Bogus index (integer / range):
        with self.assertRaises(TypeError):
            idx = pd.RangeIndex(0,1,1)
            df = pd.DataFrame(columns=['x','y','z','rx','ry','rz'], index=idx)
            afe = AFE(df)

        # TODO?: test with <2 samples

    def test_computing_features(self):
        indexes = [
            pd.Float64Index([0.0]),
            pd.DatetimeIndex(data=[dt.datetime.now()])
        ]
        window_overlap_combos = [
            [30,0],
            [15,5]
        ]
        # TODO: test other args: include_timestamp_features, include_features,
        # exclude_features, ...

        for idx in indexes:
            for window, overlap in window_overlap_combos:
                # df = pd.DataFrame(columns=['x','y','z','rx','ry','rz'], index=idx)
                afe = AFE(self.df)
                features = afe.get_features(window_size=window, overlap=overlap)

if __name__ == '__main__':
    unittest.main()
